import {
  Injectable,
  BadRequestException,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateClientDto } from './dto/create-client.dto';
import { UpdateClientDto } from './dto/update-client.dto';
import { Client } from './entities/client.entity';
import { Repository } from 'typeorm';
import { validate } from 'class-validator';
import { cryptoConfig } from 'config/cryto.config';
import { createCipheriv, createDecipheriv } from 'crypto';

@Injectable()
export class ClientsService {
  private secretKey: Buffer;
  private iv: Buffer;
  constructor(
    @InjectRepository(Client)
    private clientRepository: Repository<Client>,
  ) {
    this.secretKey = Buffer.from(cryptoConfig.secretKey, 'hex');
    this.iv = Buffer.from(cryptoConfig.iv, 'hex');
  }

  async create(createClientDto: CreateClientDto) {
    const errors = await validate(createClientDto);
    if (errors.length > 0) {
      throw new BadRequestException(errors);
    }

    const client = this.clientRepository.create(
      this.encryptDtoProperties(createClientDto),
    );
    return this.decryptDtoProperties(await this.clientRepository.save(client));
  }

  async findAll() {
    const clients = await this.clientRepository.find();
    return this.decryptDtoProperties(clients);
  }

  async findOne(id: number) {
    const client = await this.clientRepository.findOne({ where: { id } });

    if (!client) {
      throw new NotFoundException('Client not found');
    }

    return this.decryptDtoProperties(client);
  }

  async update(id: number, updateClientDto: UpdateClientDto) {
    const client = await this.clientRepository.findOne({ where: { id } });

    if (!client) {
      throw new NotFoundException('Client not found');
    }

    Object.assign(client, updateClientDto);
    return this.clientRepository.save(client);
  }

  async remove(id: number) {
    const client = await this.clientRepository.findOne({ where: { id } });
    if (!client) {
      throw new NotFoundException('Client not found');
    } else {
      return this.clientRepository.delete(id);
    }
  }

  encryptDtoProperties(dto: CreateClientDto): any {
    const encryptedDto = { ...dto };
    for (const prop in encryptedDto) {
      if (typeof encryptedDto[prop] === 'string') {
        encryptedDto[prop] = this.encrypt(encryptedDto[prop]);
      }
    }
    return encryptedDto;
  }

  decryptDtoProperties(clients: Client | Client[]): any {
    if (Array.isArray(clients)) {
      return clients.map((client) => {
        return this.decryptClientProperties(client);
      });
    } else {
      return this.decryptClientProperties(clients);
    }
  }

  private decryptClientProperties(client: Client) {
    const decryptedDto = { ...client };
    for (const prop in decryptedDto) {
      if (typeof decryptedDto[prop] === 'string') {
        decryptedDto[prop] = this.decrypt(decryptedDto[prop]);
      }
    }
    return decryptedDto;
  }

  private encrypt(text: string): string {
    const cipher = createCipheriv('aes-256-cbc', this.secretKey, this.iv);
    let encrypted = cipher.update(text, 'utf8', 'hex');
    encrypted += cipher.final('hex');
    return encrypted;
  }

  private decrypt(encryptedText: string): string {
    const decipher = createDecipheriv('aes-256-cbc', this.secretKey, this.iv);
    let decrypted = decipher.update(encryptedText, 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    return decrypted;
  }
}

import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  NotFoundException,
  Res,
  Logger,
} from '@nestjs/common';
import { ClientsService } from './clients.service';
import { CreateClientDto } from './dto/create-client.dto';
import { UpdateClientDto } from './dto/update-client.dto';
import { Response } from 'express';

@Controller('clients')
export class ClientsController {
  constructor(private readonly clientsService: ClientsService) {}

  @Post()
  async create(@Body() createClientDto: CreateClientDto, @Res() res: Response) {
    try {
      const createdClient = await this.clientsService.create(createClientDto);
      res.status(201).json(createdClient);
    } catch (error) {
      res.status(500).json({ message: 'Internal server error' });
    }
  }

  @Get()
  async findAll(@Res() res: Response) {
    try {
      const clients = await this.clientsService.findAll();
      return res.status(200).json(clients);
    } catch (error) {
      res.status(500).json({ message: 'Internal server error' });
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string, @Res() res: Response) {
    try {
      const client = await this.clientsService.findOne(+id);
      if (!client) {
        res.status(404).json({ message: 'Client not found' });
      } else {
        res.status(200).json(client);
      }
    } catch (error) {
      if (error instanceof NotFoundException) {
        res.status(404).json({ message: 'Client not found' });
      } else {
        res.status(500).json({ message: 'Internal server error' });
      }
    }
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateClientDto: UpdateClientDto,
    @Res() res: Response,
  ) {
    try {
      const updatedClient = await this.clientsService.update(
        +id,
        updateClientDto,
      );
      res.status(200).json(updatedClient);
    } catch (error) {
      if (error instanceof NotFoundException) {
        res.status(404).json({ message: 'Client not found' });
      } else {
        res.status(500).json({ message: 'Internal server error' });
      }
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string, @Res() res: Response) {
    try {
      await this.clientsService.remove(+id);
      res.status(200).json({ message: 'Client deleted' });
    } catch (error) {
      if (error instanceof NotFoundException) {
        res.status(404).json({ message: 'Client not found' });
      } else {
        res.status(500).json({ message: 'Internal server error' });
      }
    }
  }
}

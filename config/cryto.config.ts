import * as dotenv from 'dotenv';

dotenv.config();

export const cryptoConfig = {
  secretKey: process.env.SECRET_KEY || 'default-secret-key',
  iv: process.env.IV || 'default-initialization-vector',
};
